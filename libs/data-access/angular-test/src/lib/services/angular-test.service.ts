import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class AngularTestService {

  public random: number;

  public testSubject$ = new BehaviorSubject<number>(0);

  constructor() {
    this.random = Math.round(Math.random() * 1000);
    this.testSubject$.subscribe((value) => {
      console.log('VALUE: ', value);
    });
  }
}

import { Component, ChangeDetectionStrategy } from '@angular/core';
import { AngularTestService } from '../../services/angular-test.service';

@Component({
  selector: 'eurobank-angular-test',
  templateUrl: './angular-test.component.html',
  styleUrls: ['./angular-test.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AngularTestComponent {
  public test = 0;

  constructor(public angularTestService: AngularTestService) {
    this.angularTestService.testSubject$.subscribe((value) => {
      this.test = value;
    });
  }

  increaseButton() {
    this.angularTestService.testSubject$.next(this.test + 1);
  }
}

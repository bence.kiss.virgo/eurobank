import { ModuleWithProviders, NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AngularTestService } from './services/angular-test.service';
import { AngularTestComponent } from './components/angular-test/angular-test.component';

@NgModule({
  imports: [CommonModule],
  providers: [AngularTestService],
  declarations: [AngularTestComponent],
  exports: [AngularTestComponent],
})
export class DataAccessAngularTestModule {

  private static testService: AngularTestService = new AngularTestService();

  constructor(@Optional() @SkipSelf() parentModule?: DataAccessAngularTestModule) {
    if (parentModule) {
      console.log('DataAccessAngularTestModule is already loaded. Import it in the AppModule only');
      //throw new Error(
      //  'DataAccessAngularTestModule is already loaded. Import it in the AppModule only');
    }
  }

  static forRoot(): ModuleWithProviders<DataAccessAngularTestModule> {
    return {
      ngModule: DataAccessAngularTestModule,
      providers: [
        { provide: AngularTestService, useValue: this.testService }
      ]
    };
  }


  static forChild(): ModuleWithProviders<DataAccessAngularTestModule> {
    return {
      ngModule: DataAccessAngularTestModule,
      providers: [
        { provide: AngularTestService, useValue: this.testService },
      ]
    };
  }

}

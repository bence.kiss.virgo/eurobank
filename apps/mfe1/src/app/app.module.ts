import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import {
  DataAccessAngularTestModule,
} from '@eurobank/data-access/angular-test';

@NgModule({
  declarations: [AppComponent],
  imports: [BrowserModule, AppRoutingModule, DataAccessAngularTestModule.forChild()],
  // providers: [AngularTestService],
  bootstrap: [AppComponent],
})
export class AppModule {}

import { Component } from '@angular/core';
import { AngularTestService } from '@eurobank/data-access/angular-test';

@Component({
  selector: 'eurobank-mfe1',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'mfe1';

  public test = 0;

  constructor(public angularTestService: AngularTestService) {
    this.angularTestService.testSubject$.subscribe((value) => {
      this.test = value;
    });
  }

  increaseButton() {
    this.angularTestService.testSubject$.next(this.test + 1);
  }
}

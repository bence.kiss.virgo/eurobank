import { NgModule } from '@angular/core';
import { RouterModule, Route } from '@angular/router';
import { loadRemoteModule } from '@angular-architects/module-federation';

const routes: Route[] = [
  {
    path: '',
    outlet: 'mfe1',
    loadChildren: () =>
      loadRemoteModule({
        // remoteEntry: 'http://localhost:5000/remoteEntry.js',
        remoteName: 'mfe1',
        exposedModule: './Module',
      }).then((m) => m.AppModule),
  },
  {
    path: '',
    outlet: 'mfe1-copy',
    loadChildren: () =>
      loadRemoteModule({
        // remoteEntry: 'http://localhost:5000/remoteEntry.js',
        remoteName: 'mfe1',
        exposedModule: './Module',
      }).then((m) => m.AppModule),
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}

import { Component, OnInit } from '@angular/core';
import { MirageService } from './services/mirage.service';
import { TestApiService } from './services/api/test.service';
import { AngularTestService } from '@eurobank/data-access/angular-test';

@Component({
  selector: 'eurobank-main',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  title = 'main';

  public test = 0;

  constructor(
    private mirageService: MirageService,
    private testApiService: TestApiService,
    public angularTestService: AngularTestService,
  ) {
    this.mirageService.create();

    this.angularTestService.testSubject$.subscribe((value) => {
      this.test = value;
    });
  }

  increaseButton() {
    this.angularTestService.testSubject$.next(this.test + 1);
  }

  ngOnInit() {
    this.testApiService.getTestExample().subscribe({
      next: (test: any) => console.log(test),
      error: (error: Error) => console.error(error),
      complete: () => console.log('Request completed!'),
    });
  }
}

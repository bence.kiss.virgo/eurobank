import { Injectable } from '@angular/core';
import { createServer } from 'miragejs';
import { Test } from '../../mirage/endpoints/test';

@Injectable({
  providedIn: 'root',
})
export class MirageService {
  create() {
    createServer({
      routes() {
        this.namespace = 'api';

        this.get('/test', Test);
      },
    });
  }
}
